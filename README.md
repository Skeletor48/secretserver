   # Secret Server
   
   The task was:
   
  - To implement a secret server. The secret server can be used to store and share secrets using a random generated URL. 
  - But the secret can be read only a limited number of times after that it will expire and won’t be available. 
 -  The secret may have a TTL (Time to live). After the expiration time the secret won’t be available anymore.
  - Build a minimal, but functional frontend in VueJS with the ability to create and view secrets (if the hash is known)
  
### Tech
The used technologies are:

* [node.js] - v12.19.0
* [Express] - v4.17.1
* [vue.js] - v2.6.11
* [mongoDB] - v4.4.1
* [mongoose] - v5.10.9

### Installation

#### Method 1: Start up in localhost

The *secretserver* Project folder contains two sub-folders. One is *client* one is *server*. Both has it's own package json.
After cloning the repository:
```sh
$ git clone https://gitlab.com/Skeletor48/secretserver.git
```

Enter the sub-folders and install the packages:
*client:*
```sh
$ cd secretserver
$ cd client
$ npm i
```
*server:*
```sh
$ cd ..
$ cd server
$ npm i
```

Before you start up the applications, first you have to create a *.env* file based on the *.env.example* file that you find in the root folder of the server app.

```
MONGODB_URL= your local mongo instance where the secrets collection exists 
MONGODB_URL_CLOUD= or you could use a cloud instance from Atlas for example
PORT= use your favourite port
SECRET_KEY= use any 32 character long key, we will use it at the encryption
```

Now you should start the server:

```sh
$ cd server
$ npm run dev
```

and the client:

```sh
$ cd ..
$ cd client
$ npm run serve
```

> HINT! If the running client can not communicate with the running server, please check the baseURL in 
> `client/src/http-common.js`. The PORT for AXIOS is set to 3003. It can be anything else, but it has 
> to be the same you provided in the .env file for the server.


#### Method 2: The Docker way

First pull the image from DockerHub:

```sh
$ docker pull skeletor48/om-secret-server:latest
```

Now we can run it:

```sh
$ docker run --env-file=.env -p 3003:3000 -d skeletor48/om-secret-server:latest
```

> HINT! After the *-p* port parameter the first number is the host's port where we expose our image's port 
> defined in the .env file. So this has to be the same like we have in `client/src/http-common.js`. And if you want to run the client from a
> docker image also, than I have bad news. I've hardcoded the 3003 port. :( 

> AAAND ANOTHER HINT! Make sure you provide a valid .env file in a right path. A good workaround to run the command from the same 
> diretory where we have the .env file. Also it is VERY IMPORTANT to note, that in this image I use the *MONGODB_URL_CLOUD*
> enviromental variable, cause it was way easier for me to get a working docker image with a cloud mongo provider than with a local one.

Ok so far I hope everything went fine. Let's get our client up!

```sh
$ docker pull skeletor48/om-secret-client:latest
```

```sh
$ docker run -p < your favourite port, I used 8060 >:8040 -d skeletor48/om-secret-client:latest
```

Fingers crossed... now in your browser on http://localhost:8060/ the app should run. 


### Use te app


The app has three screens:

- */  &  /introduction* :  Just a quick introduction
 ![](images/image1.png)

- */add* :  Press the *Tell a Secret* button, here you can add the secret, 
 enter a number that represents how many times the url can be checked 
 and a time window in minutes while the link will be available

 ![](images/image5.png)

 After pressing the upload button, you will get a hash. The hash is copied to dashboard on click.

 ![](images/image2.png)


- */secrets* :  Press the *Check a Secret* button, here you can add the hash, 
 and check the decrypted string value of it. But only it is not expired or reached the view limit.

 ![](images/image3.png)

 ![](images/image4.png)


### Notes

- The database only stores hashed values, the encryption/decryption is handled by the server using Node.js's *crypto* package. 
- The time window and the exact number of views is handled by the *mongoose* query itself. However I have concerns if I hardly 
  hurt idempotency or not... :smirk:
- There are no proper error handling in the project, also a good logger class would be great to implement
- Besides theese I think about to implement a middleware or a worker that supposed to handle the expired secrets and hard delete them
- I have to mention the obvious fact that I did not implement any tests. :sweat: I am truly ashamed.
- During the contanerization process I made some mistakes, but I rather left them in working state (runs in development mode, hardcoded url...)


### Resources


| Topic | Link |
| ------ | ------ |
| The node server structure based on this article | [https://softwareontheroad.com/ideal-nodejs-project-structure/]|
| This tutoprial was a big help with REST API and Express | [https://dev.to/beznet/build-a-rest-api-with-node-express-mongodb-4ho4?utm_source=additional_box&utm_medium=internal&utm_campaign=regular&booster_org=]|
| I used this source to implement encryption| [https://attacomsian.com/blog/nodejs-encrypt-decrypt-data]|
| For the client I deeply related on this article | [https://bezkoder.com/vue-js-crud-app/]|




[node.js]: <http://nodejs.org>
[express]: <http://expressjs.com>
[vue.js]: <https://vuejs.org/>
[mongoDB]: <https://www.mongodb.com/>
[mongoose]: <https://mongoosejs.com/>