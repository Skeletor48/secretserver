import SecretModel from "../models/secret.js";
import encryptor from "../utils/encryptor.js";
import timeHandler from "../utils/timeHandler.js";

export default class SecretService {
    createNewSecret = async (secretDTO) => {
        const newSecret = await SecretModel.create({
            hash: encryptor.encryptHash(secretDTO.secret),
            expiresAt: timeHandler.addMinutes(secretDTO.expireAfter),
            remainingViews: secretDTO.expireAfterViews,
        });
        // Get rid of unneccessary properties via
        // the custom toJSON method declared on the Schema
        const newSecretObject = newSecret.toJSON();

        return Object.assign(newSecretObject, { secretText: secretDTO.secret });
    };

    findAndUpdateSecretByHash = async (hash) => {
        const secretRecord = await SecretModel.findOneAndUpdate(
            {
                hash: hash,
                remainingViews: { $gt: 0 },
                expiresAt: { $gt: Date() },
            },
            {
                $inc: {
                    remainingViews: -1,
                },
            },
            // Send back the updated document
            { new: true }
        );

        if (!secretRecord) {
            return null;
        }
        // Get rid of unneccessary properties via
        // the custom toJSON method declared on the Schema
        const newSecretObject = secretRecord.toJSON();

        return Object.assign(newSecretObject, {
            secretText: encryptor.decryptHash(secretRecord.hash),
        });
    };
}
