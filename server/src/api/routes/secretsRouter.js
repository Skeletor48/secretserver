import secretService from "../../services/secretService.js";

export default (router) => {
    const SecretService = new secretService();

    router.get("/:hash", async (req, res) => {
        const hash = req.params.hash;
        let secretRecord;
        try {
            secretRecord = await SecretService.findAndUpdateSecretByHash(hash);
            if (!secretRecord) {
                return res
                    .status(404)
                    .json({ message: `Cant find secret with hash: ${hash}` });
            }
        } catch (err) {
            console.error.bind(console, "Database error:");
            return res.status(500).json({ message: err.message });
        }

        res.json(secretRecord);
    });

    router.post("/", async (req, res) => {
        const secretDTO = req.body;

        try {
            const newSecret = await SecretService.createNewSecret(secretDTO);
            res.status(201).json(newSecret);
            console.log("Secret created Successfully");
        } catch (error) {
            console.error.bind(console, "Input error:");
            res.status(400).json({ message: error.message });
        }
    });
};
