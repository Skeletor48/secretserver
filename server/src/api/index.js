import express from "express";
import secretsRouter from "./routes/secretsRouter.js";

export default () => {
    const router = express.Router();
    router.use("/secret", router);
    secretsRouter(router);

    return router;
};
