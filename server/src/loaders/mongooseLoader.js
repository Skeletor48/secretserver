import mongoose from "mongoose";
import config from "../config/index.js";

export default async () => {
    const connection = await mongoose.connect(config.databaseURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    mongoose.set("useFindAndModify", false);

    return connection.connection.db;
};
