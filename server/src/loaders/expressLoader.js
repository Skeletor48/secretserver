import express from "express";
import cors from "cors";
import routes from "../api/index.js";
import config from "../config/index.js";

export default (app) => {
    app.use(cors());
    app.use(express.json());
    // Load API routes
    app.use(config.api.prefix, routes());

    /// catch 404 and forward to error handler
    app.use((req, res, next) => {
        const err = new Error("Not Found");
        err["status"] = 404;
        next(err);
    });

    /// error handler
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });
    });
};
