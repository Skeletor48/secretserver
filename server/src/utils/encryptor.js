import crypto from "crypto";
import config from "../config/index.js";

const algorithm = "aes-256-ctr";
const secretKey = config.secretKey;

const encryptHash = (text) => {
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

    return iv.toString("hex") + encrypted.toString("hex");
};

const decryptHash = (hash) => {
    const iv = hash.substr(0, 32);
    const content = hash.substring(32);

    const decipher = crypto.createDecipheriv(
        algorithm,
        secretKey,
        Buffer.from(iv, "hex")
    );

    const decrpyted = Buffer.concat([
        decipher.update(Buffer.from(content, "hex")),
        decipher.final(),
    ]);

    return decrpyted.toString();
};

export default { encryptHash, decryptHash };
