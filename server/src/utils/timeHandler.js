const addMinutes = (minutes) => {
    if (!minutes) {
        // JavaScript max date, representing infinity
        return new Date(8640000000000000);
    }

    const currentTime = new Date();
    return new Date(currentTime.getTime() + minutes * 60000);
};

export default { addMinutes };
