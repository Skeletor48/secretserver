import config from "./config/index.js";
import express from "express";
import expressLoader from "./loaders/expressLoader.js";
import mongooseLoader from "./loaders/mongooseLoader.js";

async function startDb() {
    try {
        await mongooseLoader();
    } catch (error) {
        // If there is no DB connection, we terminate the app
        console.error("Connection error: ", error);
        process.exit(1);
    }
    console.log(`
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    🌿 Connected To MongoDB on uri: ${"\x1b[33m"}${config.databaseURL}${"\x1b[0m"} 🌿
    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    `);

    startServer();
}

async function startServer() {
    const app = express();

    await expressLoader(app);

    app.listen(config.port, () => {
        console.log(`
        +++++++++++++++++++++++++++++++++++++++++++
        💀 48  Server listening on port: ${"\x1b[35m"}${config.port}${"\x1b[0m"}  48 💀
        +++++++++++++++++++++++++++++++++++++++++++
      `);
    }).on("error", (err) => {
        console.log("Server is not running because of: ", err);
        process.exit(1);
    });
}

startDb();
