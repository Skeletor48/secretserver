import mongoose from "mongoose";

const secretSchema = new mongoose.Schema({
    hash: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    expiresAt: {
        type: Date,
        default: 0,
    },
    remainingViews: {
        type: Number,
        required: true,
        default: 0,
    },
});

secretSchema.methods.toJSON = function () {
    var obj = this.toObject();
    delete obj._id;
    delete obj.__v;
    return obj;
};

const Secret = mongoose.model("Secret", secretSchema);

export default Secret;
