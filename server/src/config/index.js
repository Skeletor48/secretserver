import dotenv from 'dotenv';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (envFound.error) {
    // This error should stop the process

    throw new Error("⚠️  Couldn't find .env file  ⚠️")
}

export default {
    port: parseInt(process.env.PORT, 10),
    databaseURL: process.env.MONGODB_URL_CLOUD,
    api: {
        prefix: "/api",
    },
    /// It has to be exactly 32character long
    secretKey: process.env.SECRET_KEY,
};
