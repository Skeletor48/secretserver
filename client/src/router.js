import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/secrets",
      name: "secrets",
      component: () => import("./components/SecretChecker")
    },
    {
      path:"/",
      alias: "/introduction",
      name: "secret-server-details",
      component: () => import("./components/Introduction")
    },
    {
      path: "/add",
      name: "add-secret",
      component: () => import("./components/SecretForm")
    }
  ]
});