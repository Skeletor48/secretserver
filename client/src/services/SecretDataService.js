import http from "../http-common.js";

class SecretDataService {

  create(data) {
    return http.post("/secret", data);
  }

  findByHash(hash) {
    return http.get(`/secret/${hash}`);
  }
}

export default new SecretDataService();